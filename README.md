[TOC]

## 前提条件
本ansibleでは**6台を1セット**としてScaleIO構築を行います。
6台それぞれで使用する**ストレージデバイスの数、サイズは共通**とします
実行手順は以下を参照して下さい

## インベントリ変更
inv.iniに対象の6台のIPアドレスを記載して下さい
記載例
```
[all]
192.168.33.15
192.168.33.16
192.168.33.17
192.168.33.18
192.168.33.19
192.168.33.20
```

## 変数定義変更
roles/scaleio/vars/main.ymlの変数定義を適宜変更して下さい
以下のcluster、nodes、devicesが都度変更となります。
(nodesやdevicesは構成に合わせて行を追加して下さい)
記載例
```
cluster:
	env: prd
	pd_name: yb-prd-001
	pool_name: ssd-pool01
	mdm1_ip: 192.168.33.15
	mdm2_ip: 192.168.33.16
	tb_ip: 192.168.33.17
	sds1_ip: 192.168.33.18
	sds2_ip: 192.168.33.19
	stock_ip: 192.168.33.20
nodes:
	{ ip: "{{ cluster.mdm1_ip }}" , fs: fs01 }
	{ ip: "{{ cluster.mdm2_ip }}" , fs: fs02 }
	{ ip: "{{ cluster.tb_ip }}" , fs: fs03 }
	{ ip: "{{ cluster.sds1_ip }}" , fs: fs03 }
	{ ip: "{{ cluster.sds2_ip }}" , fs: fs03 }
	{ ip: "{{ cluster.stock_ip }}" , fs: fs03 }
```

## ansible-playbook
プレイブックを実行します
bash ansible-playbook -i inv.ini site.yml -vvv

## 確認
今後追記(自動化予定)


